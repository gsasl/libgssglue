# CONTRIBUTING

We welcome your contributions to libgssglue!

Development is coordinated on the GitLab project and you may open
issues or merge requests:

https://gitlab.com/gsasl/libgssglue/

Your contribution is assumed to be under a BSD-3-Clause license, see
[COPYING](COPYING).  Contributor must certify the [Developer
Certificate of Origin](https://developercertificate.org/) for their
contribution by adding the Signed-off-by: tag to their submission.

## Release process

- Confirm that the version number in configure.ac's AC_INIT is
  updated, and that NEWS covers relevant changes for the release.

- Verify that you have pushed everything to git master branch and that
  the pipeline passes: https://gitlab.com/gsasl/libgssglue/-/pipelines

- Tag your release.

```
git tag -s -m "libgssglue 0.9" libgssglue-0.9
```

- Build a tarball from a clean checkout and sign it.

```
git clean -d -x -f; git restore --worktree --staged .
./bootstrap
./configure
make distcheck
gpg -b libgssglue-*.tar.gz
```

- Push the tag.

```
git push origin libgssglue-0.9
```

- Create a release on the GitLab page:
  https://gitlab.com/gsasl/libgssglue/-/releases
  Click on "New release" and select your tag.
  Use a release title identical to the tag name, i.e., "libgssglue-0.9".
  In the release notes, upload the tarball and tarball signature.
